import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import TimeTable from '@/components/TimeTable'
import * as COMMON from '../common'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/timetable',
      name: 'TimeTable',
      component: TimeTable,
      beforeEnter: (to, from, next) => {
          if (COMMON.user._name !== 'none') {
            import('../assets/index.js')
              next();
          } else {
              next({path: '/'});
          }
      }
    }
  ]
})


