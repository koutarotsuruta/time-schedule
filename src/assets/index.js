import axios from 'axios'
import Vue from 'vue'

function get_today_time(last_tr) {
  let TODAY_DATE = last_tr.children[0].innerHTML,
      START_DATE = last_tr.children[1].innerHTML,
      END_DATE = last_tr.children[2].innerHTML,
      REST_DATE = last_tr.children[3].innerHTML,
      LATE_REST_DATE = last_tr.children[4].innerHTML,
      WORKING_HOURS = last_tr.children[5].innerHTML,
      OVERTIME_WORK = last_tr.children[6].innerHTML;

    return [TODAY_DATE,START_DATE,END_DATE,REST_DATE,LATE_REST_DATE,WORKING_HOURS,OVERTIME_WORK];
}
// calculate time
function diff_time(start, end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
       hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
}

// init
axios.get('https://ntp-a1.nict.go.jp/cgi-bin/json')
.then(function (response) {
  const TM = new Date( response.data.st * 1000 );
     console.log(TM);
  const today = TM.getDate();

  //今日のタイムテーブルが追加してあるかを確認
  for(let i=0; i<document.getElementsByTagName('tr').length; i++) {
    if(Number(document.getElementsByTagName('tr')[i].children[0].innerHTML) === today) {
      break;
    }
    if(i === document.getElementsByTagName('tr').length-1) {
      document.getElementById('time-table').innerHTML += '<tr><td>'+ TM.getDate() +'</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
      break;
    }
  }
})
.catch(function (error) {
  console.log(error);
});

document.getElementById('in-work').addEventListener('click',function(){

  axios.get('https://ntp-a1.nict.go.jp/cgi-bin/json')
    .then(function (response) {
      const TM = new Date( response.data.st * 1000 );
      let LAST_TR_LENGTH = document.getElementsByTagName('tr').length - 1,
      last_tr = document.getElementsByTagName('tr')[LAST_TR_LENGTH];

      let time_table = get_today_time(last_tr);

        // last_tr;
        last_tr.innerHTML = '';

        // replace time
        Vue.component('todo-item', {
          props: ['date'],
          template: '<td>{{ date.time }}</td>'
        });

        last_tr.innerHTML += '<todo-item v-for="item in groceryList" v-bind:date="item"></todo-item>';
        new Vue({
          el: '#time-table',
          data: {
            groceryList: [
              { time: time_table[0] },
              { time: TM.getHours()+':'+TM.getMinutes() },
              { time: time_table[2] },
              { time: time_table[3] },
              { time: time_table[4] },
              { time: time_table[5] },
              { time: time_table[6] }
            ]
          }
        });
    })
    .catch(function (error) {
      console.log(error);
    });
});
document.getElementById('out-work').addEventListener('click',function(){
  axios.get('https://ntp-a1.nict.go.jp/cgi-bin/json')
    .then(function (response) {
      const TM = new Date( response.data.st * 1000 );
      let LAST_TR_LENGTH = document.getElementsByTagName('tr').length - 1,
      last_tr = document.getElementsByTagName('tr')[LAST_TR_LENGTH];

      let time_table = get_today_time(last_tr);
      let START_DATE = time_table[1];
      let END_DATE = TM.getHours()+':'+TM.getMinutes();
      let DIFF_TIME = diff_time(START_DATE,END_DATE);

        // last_tr;
        last_tr.innerHTML = '';

        // replace time
        Vue.component('todo-item', {
          props: ['date'],
          template: '<td>{{ date.time }}</td>'
        });
        last_tr.innerHTML += '<todo-item v-for="item in groceryList" v-bind:date="item"></todo-item>';
        new Vue({
          el: '#time-table',
          data: {
            groceryList: [
              { time: time_table[0] },
              { time: time_table[1] },
              { time: TM.getHours()+':'+TM.getMinutes() },
              { time: time_table[3] },
              { time: time_table[4] },
              { time: DIFF_TIME },
              { time: time_table[6] }
            ]
          }
        });
    })
    .catch(function (error) {
      console.log(error);
    });
});