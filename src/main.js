// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

import firebase from 'firebase'
// Initialize Firebase
var config = {
	apiKey: "AIzaSyANeRPUyYcPner_8Da1eLuWR838KV0NL3c",
	authDomain: "vuejs-firebase-01-49436.firebaseapp.com",
	databaseURL: "https://vuejs-firebase-01-49436.firebaseio.com",
	projectId: "vuejs-firebase-01-49436",
	storageBucket: "vuejs-firebase-01-49436.appspot.com",
	messagingSenderId: "1033671560203"
};
firebase.initializeApp(config);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
